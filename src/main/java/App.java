
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App extends Application {
	private static Logger LOG = LoggerFactory.getLogger(App.class);
	private static String hello = "Hello, World!"; 
	
	@Override
	public void start(Stage stage) {
		LOG.trace("enter start method");

		TextField tf = new TextField(hello);
		stage.setScene(new Scene(tf));

		stage.setTitle(hello);
		stage.show();
		
		LOG.trace("exit start method");
	}

	public static void main(String... args) {
		LOG.info("starting");
		launch(args);
		LOG.info("finishing");
	}
}
