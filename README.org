a cross-platform javafx program for reading

maybe even mobile!

we use maven to build the project

Run `mvn clean package exec:java -D exec.mainClass=App` to execute

and if you don't have java or maven installed, but do have docker
`docker run -it --rm --name reader-build -v "$PWD":/usr/src/reader -w /usr/src/reader maven mvn clean package exec:java -D exec.mainClass=App` should work.
